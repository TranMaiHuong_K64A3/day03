<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="main.css">
    </head>
    <body>
        <div class="container">
            <div class="container__content">
                <div class="form__item">
                    <label for="form__name" class="form__label">Họ và tên</label>
                    <input type="text" class="form__input" id="form__name">
                </div>
                <div class="form__item">
                    <div class="form__label">Giới tính</div>
                    <!-- for loop -->
                    <?php
                        $gender = array(
                            0 => "Nam", 
                            1 => "Nữ"    
                        );
                        for($i = 0; $i < count($gender); $i++) {
                            echo    "<label class='form__gender'>
                                        $gender[$i]
                                        <input type='radio' name='gender' class='form__input input__gender'>
                                        <span class='checkmark'></span>
                                    </label>";
                        }
                    ?>
                </div>
                <div class="form__item">
                    <label for="form__select" class="form__label">Phân khoa</label>
                    <div class="select">
                        <select name="form__select" id="form__select" class="form__input form__select">
                            <!-- foreach loop -->
                            <?php
                                $classes = array(
                                    "MAT" => "Khoa học máy tính",
                                    "KDL" => "Khoa học vật liệu"
                                );
                                echo "<option value=''></option>";
                                foreach($classes as $class => $val) {
                                    echo "<option value='$class'>$val</option>";
                                }
                            ?>
                        </select>
                        <span class="focus"></span>
                    </div>
                </div>
                <input type="submit" value="Đăng ký" class="form__submit">
            </div>
        </div>

    </body>
</html>